import { getRequestData } from "../api/request";
import { useLocation } from "react-router-dom";

function TableComponent({ requestData, setRequestData }) {
  let location = useLocation();
  const handleChange = (evt) => {
    if (
      location &&
      location.pathname &&
      location.pathname !== "/" &&
      location.pathname !== ""
    ) {
      getRequestData(setRequestData, `?${evt.target.name}=${evt.target.value}`);
    } else {
      getRequestData(
        setRequestData,
        `/councillors?${evt.target.name}=${evt.target.value}`
      );
    }
  };
  return (
    <div className="form">
      <label>Sort by:</label>
      <select name="sortBy" onChange={(e) => handleChange(e)}>
        {requestData &&
          requestData.length &&
          Object.keys(requestData[0].header).map((key, index) => (
            <option key={index} value={key}>
              {requestData[0].header[key]}
            </option>
          ))}
      </select>

      <label>Filter by:</label>
      <input
        type="text"
        id="filter"
        name="filterBy"
        placeholder="Enter input"
        onKeyPress={(e) => e.key === "Enter" && handleChange(e)}
      />
    </div>
  );
}

export default TableComponent;
