import { useEffect } from "react";
import { useLocation } from "react-router-dom";

import { getRequestData } from "../api/request";

function TableComponent({ requestData, setRequestData }) {
  let location = useLocation();
  useEffect(() => {
    if (location && location.pathname && location.pathname !== "/") {
      getRequestData(setRequestData, location.pathname + location.search);
    } else {
      getRequestData(setRequestData, "/councillors");
    }
  }, [setRequestData, location, location.pathname]);
  return (
    <div>
      {requestData && requestData.length && !requestData.message ? (
        <table id="table_data">
          <thead>
            <tr>
              {requestData[0] &&
                requestData[0].header &&
                Object.keys(requestData[0].header).map(
                  (key, index) =>
                    key !== "header" && (
                      <th key={index}>{requestData[0].header[key]}</th>
                    )
                )}
            </tr>
          </thead>
          {requestData.map(
            (data, i) =>
              data && (
                <tbody>
                  <tr key={i}>
                    {Object.keys(data).map(
                      (key, index) =>
                        key !== "header" && <td key={index}>{data[key]}</td>
                    )}
                  </tr>
                </tbody>
              )
          )}
        </table>
      ) : (
        <span className="loading"> Loading table...</span>
      )}
    </div>
  );
}

export default TableComponent;
