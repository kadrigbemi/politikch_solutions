import { useState } from "react";
import { BrowserRouter as Router } from "react-router-dom";

import MenuBar from "./component/MenuBar";
import TableComponent from "./component/Table";
import FormComponent from "./component/Form";

import "./App.css";

function App() {
  const [requestData, setRequestData] = useState([]);

  return (
    <Router>
      <>
        <MenuBar />
        <FormComponent
          requestData={requestData}
          setRequestData={setRequestData}
        />
        <TableComponent
          requestData={requestData}
          setRequestData={setRequestData}
        />
      </>
    </Router>
  );
}

export default App;
