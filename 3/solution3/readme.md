
Steps to run application:
 Use `npm i` to install dependencies 
 Use `npm start` to run application 

 Optional:
 You can combine both commands when you are loading the application for the first time. Then you will use `npm i && npm start`

 More information:
The server will run on port `http://127.0.0.1:3001/` it will use `http://ws-old.parlament.ch/councillors/` as its base url and `http://ws-old.parlament.ch/` as the base url to route to different paths of the application.

Important:
Solution 3 is dependent on the server from solution1 app so in order to use solution3 please make sure that solution1 is running in the background.