const http = require("http");
const url = require("url");
const request = require("./api/request");

const hostname = "127.0.0.1";
const port = 3001;

const defaultServerURL = "http://ws-old.parlament.ch";

const server = http.createServer(async (req, res) => {
  const queryObject = url.parse(req.url, true).query;
  let getRequestUrl =
    req.url === "/"
      ? `${defaultServerURL}/Councillors`
      : `${defaultServerURL}${req.url}`;

  res.setHeader("Content-Type", "application/json");
  const result = await request.handleGetRequest(getRequestUrl, queryObject);
  res.end(JSON.stringify(result, null, 3));
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
