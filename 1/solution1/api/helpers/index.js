const getHeaders = (nodeList) => {
  let res;
  for (let t = 0; t < nodeList.length; t++) {
    const parent = nodeList[t].parentElement;
    const isInformationTable = parent.classList.contains("information");
    if (!isInformationTable) {
      const firstRow = nodeList[t].querySelector("tr");
      const tableRows = nodeList[t].querySelectorAll("tr");
      const tableHeader = firstRow;
      res = { tableRows, tableHeader };
    }
  }
  return res;
};

const convertTableCellsToJSon = (headers, cells) => {
  const cellContentObj = cells.map((cell, index) => {
    const headTitle =
      headers && headers[index] && headers[index].textContent.trim();
    const cellValue = cell.textContent.replace(/[,\n+]/g, "");
    const title = headTitle && headTitle.toLowerCase();
    const getTitle = title.replace(/ +/g, "_");
    return [getTitle, cellValue.trim()];
  });
  return cellContentObj;
};

const getContents = (header, rows, queryObject) => {
  let rowsArr = rows.map((row) => {
    let rowsCells = Array.from(row.getElementsByTagName("TD"));
    let headerCells = Array.from(header.getElementsByTagName("TH"));
    if (rowsCells.length) {
      return Object.fromEntries(
        new Map(convertTableCellsToJSon(headerCells, rowsCells))
      );
    } else {
      return {
        header: Object.fromEntries(
          new Map(convertTableCellsToJSon(headerCells, headerCells))
        ),
      };
    }
  });
  let headerObj = rowsArr[0];
  if (queryObject && queryObject.filterBy) {
    const filteredData = rowsArr.filter((row) => {
      if (Object.values(row).includes(queryObject.filterBy)) {
        return Object.values(row).includes(queryObject.filterBy);
      }
    });
    rowsArr = [headerObj].concat(filteredData);
  }
  if (queryObject && queryObject.sortBy) {
    return rowsArr.sort((a, b) => {
      if (a[queryObject.sortBy] && b[queryObject.sortBy]) {
        if (queryObject.sortBy.includes("updated") || queryObject.sortBy.includes("date")) {
          return new Date(a[queryObject.sortBy]) - new Date(b[queryObject.sortBy]);
        }
        return a[queryObject.sortBy].localeCompare(b[queryObject.sortBy]);
      }
    });
  }
  return rowsArr;
};

exports.getHeaders = getHeaders;
exports.getContents = getContents;
