const { getHeaders, getContents } = require("./helpers/index");
const http = require("http");
const { JSDOM } = require("jsdom");

const handleGetRequest = async (url, queryObject) => {
  return new Promise((resolve, reject) => {
    const req = http.request(new URL(url), (res) => {
      if (res.statusCode < 200 || res.statusCode >= 300) {
        return resolve({
          statusCode: res.statusCode,
          message: "Data is unavailable on this path",
        });
      }
      let body = [];
      res.on("data", function (chunk) {
        body.push(chunk);
      });
      res.on("end", function () {
        try {
          const dom = new JSDOM(body);
          const nodes = dom.window.document.querySelectorAll("table");
          if (nodes) {
            if (getHeaders(nodes)) {
              const { tableRows, tableHeader } = getHeaders(nodes);
              const data = getContents(tableHeader, Array.from(tableRows), queryObject);
              body = data;
            }else{
              return resolve({
                statusCode: res.statusCode,
                message: "Data is unavailable please check that the url path or query is correct",
              });
            }
          }
        } catch (e) {
          reject(e);
        }
        resolve(body);
      });
    });
    req.on("error", (e) => {
      reject(e.message);
    });
    req.end();
  });
};

exports.handleGetRequest = handleGetRequest;
