import { Link } from "react-router-dom";

function MenuBar() {
  return (
    <div className="navbar">
      <div className="dropdown">
        <button className="dropbtn">
          Councillors
          <i className="fa fa-caret-down"></i>
        </button>
        <div className="dropdown-content">
          <Link to="/councillors/basicdetails">Basic Details</Link>
          <Link to="/councillors/historic">Historic</Link>
        </div>
      </div>
      <Link to="/affairsummaries">Affairs</Link>
      <div className="dropdown">
        <button className="dropbtn">
          Affairs summaries
          <i className="fa fa-caret-down"></i>
        </button>
        <div className="dropdown-content">
          <Link to="/affairs/types">Types</Link>
          <Link to="/affairs/states">States</Link>
          <Link to="/affairs/topics">Keywords</Link>
          <Link to="/affairs/descriptors">Descriptors</Link>
        </div>
      </div>
    </div>
  );
}

export default MenuBar;
