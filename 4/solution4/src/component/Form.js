import { getRequestData } from "../api/request";
import { useLocation } from "react-router-dom";
import Selector from "./Selector";

function TableComponent({ requestData, setRequestData }) {
  let location = useLocation();
  const handleChange = (evt) => {
    if (
      location &&
      location.pathname &&
      location.pathname !== "/" &&
      location.pathname !== ""
    ) {
      getRequestData(setRequestData, `?${evt.target.name}=${evt.target.value}`);
    } else {
      getRequestData(
        setRequestData,
        `/councillors?${evt.target.name}=${evt.target.value}`
      );
    }
  };
  return (
    <div className="form">
      <Selector
        onChangeFunc={(e) => handleChange(e)}
        label="Sort by:"
        data={requestData}
        name="sortBy"
      />

      <label>Filter by:</label>
      <input
        type="text"
        id="filter"
        name="filterBy"
        placeholder="Enter input"
        onKeyPress={(e) => e.key === "Enter" && handleChange(e)}
      />
    </div>
  );
}

export default TableComponent;
