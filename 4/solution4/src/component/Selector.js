function Selector({ data, label, onChangeFunc, name, defaultValue }) {
  return (
    <>
      <label>{label}</label>
      <select name={name} onChange={onChangeFunc}>
        {data &&
          data.length &&
          data[0].header &&
          Object.keys(data[0].header).map((key, index) => (
            <option key={index} value={key}>
              {data[0].header[key ? key : defaultValue]}
            </option>
          ))}
      </select>
    </>
  );
}

export default Selector;
