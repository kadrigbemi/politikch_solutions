import React, { Component } from "react";
import CanvasJSReact from "../../assets/canvasjs.react";
import Selector from "../../component/Selector";

let CanvasJSChart = CanvasJSReact.CanvasJSChart;
class ChartWithIndexLabel extends Component {
  constructor(props) {
    super(props);
    this.state = { plotData: [] };
    this.state = { xAxis: "id", yAxis: "first_name" };
  }

  render() {
    const handlePlot = (value, data, axis) => {
      let xValue = axis === "x" && value ? value : this.state.xAxis;
      let yValue = axis === "y" && value ? value : this.state.yAxis;
      if (data && data.length) {
        const mappedPlotsData = data.map((eachData) => {
          console.log(isNaN(parseInt(eachData[yValue])));
          return {
            y: isNaN(parseInt(eachData[xValue]))
              ? eachData[xValue]
              : parseInt(eachData[xValue]),
            label: isNaN(parseInt(eachData[yValue]))
              ? eachData[yValue]
              : parseInt(eachData[yValue]),
          };
        });
        this.setState(() => {
          return {
            plotData: mappedPlotsData.filter((data) => data.y && data.label),
          };
        });
      }
    };
    const options = {
      animationEnabled: true,
      exportEnabled: true,
      theme: "light2", 
      title: {
        text: "A simple bar chart for table data",
      },
      data: [
        {
          type: "bar", 
          indexLabelFontColor: "#5A5757",
          indexLabelPlacement: "outside",
          dataPoints: this.state.plotData ? this.state.plotData : [],
        },
      ],
    };

    return (
      <div className="ChartWithIndexLabel">
        <Selector
          onChangeFunc={(e) =>
            handlePlot(e.target.value, this.props.requestData, "x")
          }
          label="Select plot x-axis:"
          data={this.props.requestData}
          name="x-axis"
          defaultValue={this.state.xAxis}
        />
        <Selector
          onChangeFunc={(e) =>
            handlePlot(e.target.value, this.props.requestData, "y")
          }
          label="Select plot y-axis:"
          data={this.props.requestData}
          name="y-axis"
          defaultValue={this.state.yAxis}
        />
        <CanvasJSChart
          options={options}
        />
      </div>
    );
  }
}

export default ChartWithIndexLabel;
