import axios from "axios";

export const getRequestData = (setRequestData, url) => {
  axios(url).then(function (response) {
    setRequestData(response && response.data);
  });
};
