import { useState } from "react";
import { BrowserRouter as Router } from "react-router-dom";

import MenuBar from "./component/MenuBar";
import TableComponent from "./component/Table";
import FormComponent from "./component/Form";
import BarChart from "./component/charts/BarChart";

import "./App.css";

function App() {
  const [requestData, setRequestData] = useState([]);

  return (
    <Router>
      <>
        <MenuBar />
        <div className="chart">
          <BarChart requestData={requestData} />
        </div>
        <FormComponent
          requestData={requestData}
          setRequestData={setRequestData}
        />
        <TableComponent
          requestData={requestData}
          setRequestData={setRequestData}
        />
      </>
    </Router>
  );
}

export default App;
